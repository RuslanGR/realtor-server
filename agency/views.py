from rest_framework.generics import CreateAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import (
    UserSerializer,
    CommentSerializer,
    HousingSerializer,
)
from .models import (
    Comment,
    Housing,
)


class RegistrationView(CreateAPIView):

    permission_classes = (AllowAny,)
    serializer_class = UserSerializer


class HousingListAPIView(ListCreateAPIView):

    serializer_class = HousingSerializer
    queryset = Housing.objects.all()
    permission_classes = (AllowAny,)


class HousingDetailAPIView(RetrieveUpdateDestroyAPIView):

    serializer_class = HousingSerializer
    queryset = Housing.objects.all()
    permission_classes = (AllowAny,)


class CommentListAPIView(ListCreateAPIView):

    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        data = request.data
        housing = Housing.objects.get(pk=data['pk'])
        Comment.objects.create(user=request.user, text=data['text'], housing=housing)
        return Response()


class CommentDetailAPIView(ListCreateAPIView):

    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        housing = Housing.objects.get(pk=kwargs.get('pk'))
        comments = housing.comment_set.all()
        data = CommentSerializer(comments, many=True).data
        return Response(data)
