from django.db import models
from django.contrib.auth.models import User


class Housing(models.Model):

    title = models.CharField(max_length=128)
    image = models.ImageField(null=True, blank=True)
    description = models.TextField()
    user = models.ForeignKey(User, verbose_name='Риэлтор', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    address = models.CharField(max_length=128)


class Comment(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    housing = models.ForeignKey('Housing', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
