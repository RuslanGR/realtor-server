from django.urls import path

from .views import *


urlpatterns = (
    path('housing/', HousingListAPIView.as_view()),
    path('housing/<int:pk>/', HousingDetailAPIView.as_view()),

    path('comment/', CommentListAPIView.as_view()),
    path('comment/<int:pk>/', CommentDetailAPIView.as_view()),
)
