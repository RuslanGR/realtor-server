# Generated by Django 2.2 on 2019-04-28 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agency', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='housing',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
