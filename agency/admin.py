from django.contrib import admin

from .models import (
    Housing,
    Comment,
)


admin.site.register(Comment)
admin.site.register(Housing)
